// 树形菜单
layui.use(function () {
    var tree = layui.tree;
    // 渲染
    tree.render({
        elem: '#item1',
        isJump: true,
        data: [{
            "title": "全国计算机等级考试2023年版",
            "children": [{
                "title": "&#xe64c;全国计算机等级考试考试大纲",
                "href": "https://ncre.neea.edu.cn/html1/report/2306/266-1.htm"
            }, {
                "title": "&#xe64c;全国计算机等级考试教材目录",
                "href": "https://ncre.neea.edu.cn/html1/report/2306/236-1.htm"
            }]
        }, {
            "title": "全国计算机等级考试2022年版",
            "children": [{
                "title": "&#xe64c;全国计算机等级考试考试大纲",
                "href": "https://ncre.neea.edu.cn/html1/report/21124/245-1.htm"
            }, {
                "title": "&#xe64c;全国计算机等级考试教材目录",
                "href": "https://ncre.neea.edu.cn/html1/report/22031/1703-1.htm"
            }]
        }, {
            "title": "全国计算机等级考试2021年版",
            "children": [{
                "title": "&#xe64c;全国计算机等级考试考试大纲",
                "href": "https://ncre.neea.edu.cn/html1/report/20122/1392-1.htm"
            }, {
                "title": "&#xe64c;全国计算机等级考试教材目录",
                "href": "https://ncre.neea.edu.cn/html1/report/20122/1310-1.htm"
            }]
        }, {
            "title": "全国计算机等级考试2020年版",
            "children": [{
                "title": "&#xe64c;全国计算机等级考试考试大纲",
                "href": "https://ncre.neea.edu.cn/html1/report/19122/549-1.htm"
            }, {
                "title": "&#xe64c;全国计算机等级考试教材目录",
                "href": "https://ncre.neea.edu.cn/html1/report/19124/257-1.htm"
            }]
        }, {
            "title": "全国计算机等级考试2019年版",
            "children": [{
                "title": "&#xe64c;全国计算机等级考试考试大纲",
                "href": "https://ncre.neea.edu.cn/html1/report/18121/5452-1.htm"
            }, {
                "title": "&#xe64c;全国计算机等级考试教材目录",
                "href": "https://ncre.neea.edu.cn/html1/report/18121/4535-1.htm"
            }]
        }],
        accordion: tree,
        click: function (obj) {
            // console.log(obj.data.title); // 得到当前点击的节点数据
            let title = obj.data.title;
            let children = obj.data.children;
            if (children == undefined) {
                // layer.msg(title);
            }
        }
    });
    tree.render({
        elem: '#item2',
        isJump: true,
        data: [{
            title: '植物大战僵尸PC端',
            // spread: true, //默认展开该目录
            children: [{
                title: '&#xe64c;植物大战僵尸(终极随机版)',
                href: "https://www.123pan.com/s/woW0Vv-2NhKd.html",
            }, {
                title: "&#xe64c;植物大战僵尸(原版+修改器)",
                href: "https://www.123pan.com/s/woW0Vv-sNhKd.html",
            }],
        }, ],
        accordion: tree,
        click: function (obj) {
            // console.log(obj.data.title); // 得到当前点击的节点数据
            let title = obj.data.title;
            let children = obj.data.children;
            if (children == undefined) {
                // layer.msg(title);
            }
        }
    });
    tree.render({
        elem: '#item3',
        isJump: true,
        showLine: false,
        data: [{
            title: '工具',
            // spread: true, //默认展开该目录
            children: [{
                title: '&#xe64c;OBS录屏软件',
                href: "https://vip.123pan.cn/1812611997/%E8%BD%AF%E4%BB%B6/OBS-Studio-29.1.2-Full-Installer-x64.exe",
            }, {
                title: "&#xe64c;飞秋",
                href: "https://www.123pan.com/s/woW0Vv-YUhKd.html",
            }, {
                title: "&#xe64c;同屏投影",
                href: 'https://www.123pan.com/s/woW0Vv-GUhKd.html',
            }, {
                title: "&#xe64c;geek卸载软件",
                href: "https://www.123pan.com/s/woW0Vv-zlhKd.html",
            }],
        }],
        accordion: tree,
        click: function (obj) {
            // console.log(obj.data.title); // 得到当前点击的节点数据
            let title = obj.data.title;
            let children = obj.data.children;
            if (children == undefined) {
                // layer.msg(title);
            }
        }
    });
    tree.render({
        elem: '#item4',
        isJump: true,
        data: [{
            title: '',
            // spread: true, //默认展开该目录
            children: [{
                title: '',
                href: "",
            }, {
                title: "",
                href: "",
            }],
        }],
        accordion: tree,
        click: function (obj) {
            // console.log(obj.data.title); // 得到当前点击的节点数据
            let title = obj.data.title;
            let children = obj.data.children;
            if (children == undefined) {
                // layer.msg(title);
            }
        }
    });
});
// 锁屏功能
var password = "123456";
layui.use(function () {
    var layer = layui.layer;
    var util = layui.util;
    var form = layui.form;
    var $ = layui.$;
    // 事件
    util.on('lay-on', {
        'test-more-lockscreen': function () {
            layer.open({
                type: 1,
                title: false, // 禁用标题栏
                closeBtn: false, // 禁用默认关闭按钮
                area: ['100%', '100%'],
                scrollbar: false, // 暂时屏蔽浏览器滚动条
                anim: -1, // 禁用弹出动画
                isOutAnim: false, // 禁用关闭动画
                id: '#ID-layer-demo-inst',
                skin: 'class-demo-layer-lockscreen', // className
                content: ['<div class="layui-form">',
                    '<div class="layui-input-wrap">',
                    '<input type="password" class="class-demo-layer-pin" lay-affix="eye">',
                    '<div class="layui-input-suffix">',
                    '<i class="layui-icon layui-icon-right" id="ID-layer-demo-unlock"></i>',
                    '</div>',
                    '</div>',
                    '<div>请输入密码,方可解锁</div>',
                    '</div>'
                ].join(''),
                success: function (layero, index) {
                    var input = layero.find('input');
                    var PASS = password;
                    form.render(); // 表单组件渲染
                    input.focus();
                    // 点击解锁按钮
                    var elemUnlock = layero.find('#ID-layer-demo-unlock');
                    elemUnlock.on('click', function () {
                        if ($.trim(input[0].value) === PASS) {
                            layer.close(index);
                            layer.closeLast('dialog'); // 关闭最新打开的信息框
                            window.localStorage.suoding = "y";
                        } else {
                            layer.msg('锁屏密码输入有误', {
                                offset: '16px',
                                anim: 'slideDown'
                            })
                            input.focus();
                            window.localStorage.suoding = "n";
                        }
                    });
                    // 回车
                    input.on('keyup', function (e) {
                        var elem = this;
                        var keyCode = e.keyCode;
                        if (keyCode === 13) {
                            elemUnlock.trigger('click');
                        }
                    });
                }
            })
        },
    });
});
//轮播图
var carousel = layui.carousel;
var imgId = "#ID-carousel-demo-image";
carousel.render({
    elem: imgId,
    width: 'auto',
    height: '360px',
    interval: 5000,
    arrow: 'always',
    change: function (obj) {
        let index = obj.index;
        let a = $(imgId + " img").eq(index).attr("data-name");
        // layer.msg(a);
    },
});
var lunbotu = [];
for (let i = 0; i < lunbotu.length; i++) {
    $(imgId + " img").eq(i).attr("data-name", lunbotu[i].name);
}
$(imgId + " img").click(function () {
    let name = $(this).attr("data-name");
    if (name == "周口职业技术学院") {
        window.open("https://www.zkvtc.edu.cn/");
    }
})
$(imgId + " img").attr("alt", "加载图片时发生错误");
//顶部菜单栏
$("#tab1 li").eq(window.localStorage.tab1).click();
$("#tab1 li").click(function () {
    let index = $("#tab1 li").index(this);
    window.localStorage.tab1 = index;
})
//不负往日温柔菜单栏
$("#bufuTab li").eq(window.sessionStorage.bufuTab).click();
$("#bufuTab li").click(function () {
    let index = $("#bufuTab li").index(this);
    window.localStorage.setItem("bufuTab", index);
})
// 电影懒加载
var flow = layui.flow;
flow.lazyimg({
    elem: '#movie img',
});
//音乐懒加载
var mus = layui.flow;
mus.lazyimg({
    elem: '#music img',
})
$.ajax({
    type: 'GET',
    url: "https://vip.123pan.cn/1812611997/Be_as_gentle_as_ever/movie.json",
    success: function (res, heads, code) {
        let arr = res;
        if (typeof arr === "object") {
            // 如果返回的json是个对象
        } else {
            arr = eval("(" + arr + ")");
        }
        $("#movie").html("");
        for (let i = 0; i < arr.length; i++) {
            $("#movie").append(
                `<div class='movie-item'>
                    <h1 class="movieName">${arr[i].movieName}</h1>
                    <div class='ves'></div>
                </div>`
            );
            for (let menu = 0; menu < arr[i].movieMenu.length; menu++) {
                let src = arr[i].movieMenu[menu].src;
                let name = arr[i].movieMenu[menu].name;
                let figure = arr[i].movieMenu[menu].figure;
                let introduce = arr[i].movieMenu[menu].introduce;
                $("#movie .ves").eq(i).append(
                    `<div class='movie-menu'>
                        <a href="JavaScript:;" class="image" data-admin='${i + "," + menu}'>
                            <img lay-src="${src}" alt="${name}">
                            <span class="figure">${figure}</span>
                        </a>
                        <span class="name" data-admin='${i},${menu}'>${name}</span>
                        <span class="introduce">${introduce}</span>
                    </div>`
                );
            }
        }
    },
    error: function () {
        layer.msg(`未能成功加载电影模块`, {
            icon: 5
        });
    }
})
$("#movie").on("click", ".movie-menu .image", function () {
    let index = $(this).attr("data-admin");
    let name = index.split(",")[0];
    let src = index.split(",")[1];
    let MvSrc = arr[name].movieMenu[src].mv;
    window.sessionStorage.setItem("movieName", `${arr[name].movieMenu[src].name}`);
    window.sessionStorage.setItem("movie_src", MvSrc);
    window.sessionStorage.setItem("movie_figure", arr[name].movieMenu[src].figure);
    window.open("./else/movie.html");
})
$("#movie").on("click", ".movie-menu .name", function () {
    let index = $(this).attr("data-admin");
    let name = index.split(",")[0];
    let src = index.split(",")[1];
    let MvSrc = arr[name].movieMenu[src].mv;
    window.sessionStorage.setItem("movieName", `${arr[name].movieMenu[src].name}`);
    window.sessionStorage.setItem("movie_src", MvSrc);
    window.sessionStorage.setItem("movie_figure", arr[name].movieMenu[src].figure);
    window.open("./else/movie.html");
})
//背景音乐自动播放
document.body.addEventListener('mousedown', function () {
    var vdo = $("#aud")[0]; //jquery
    // vdo.play();
}, false);
//音乐
$.ajax({
    type: 'get',
    url: "https://vip.123pan.cn/1812611997/Be_as_gentle_as_ever/music.json",
    success: function (res, heads, code) {
        let music = res;
        if (typeof music === "object") {
            // 如果返回的json是个对象
        } else {
            music = eval("(" + music + ")");
        }
        $("#music").html("");
        for (let i = 0; i < music.length; i++) {
            $("#music").append(`
                <div class="music-menu">
                    <div class="music-image">
                        <img class="music-pic" lay-src="${music[i].music_pic}" alt="${music[i].music_username}">
                        <i class="music-begin" data-music='${music[i].music_src}'></i>
                    </div>
                    <div class="music-info">
                        <span class="music-username">${music[i].music_username}</span>
                        <span class="music-name">${music[i].music_name}</span>
                        <span class="music-timer">${musicSrc(music[i].music_src, i)}</span>
                        <span class="music-download" data-music="${music[i].music_src}">&#xe601;</span>
                    </div>
                </div>
            `);
        }
    },
    error: function () {
        layer.msg(`未能成功加载音乐模块`, {
            icon: 5
        });
    }
})
var element = layui.element;
async function musicSrc(src, i) {
    // 创建一个audio元素，并将其赋值给变量au
    var au = document.createElement('audio');
    // 将传入的src参数赋值给au的src属性
    au.src = src;
    // 初始化一个空字符串变量timer
    var timer = "";
    const pro = new Promise(function (resolve, reject) {
        // 为au元素添加一个事件监听器，当音频元数据加载完成时触发
        au.addEventListener('loadedmetadata', function () {
            // 获取音频元素的持续时间，并将其赋值给变量duration
            var duration = au.duration;
            // 将duration的值赋给timer
            timer = duration;
            // 返回timer的值
            resolve(timer)
        }, true);
    })
    return pro.then(function (result) {
        let result1 = result;
        let number = "/^[0-9]*$/";
        let min = parseInt(result1 / 60);
        let sec = parseInt(result1 % 60);
        min = min < 10 ? "0" + min : min;
        sec = sec < 10 ? "0" + sec : sec;
        $("#music .music-timer").eq(i).html(min + " : " + sec);
    });
}
const musicTitle = document.title;
var musicJindutiao = "";

function random() {
    let arrRandom = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
    let str = "";
    for (let i = 0; i < 8; i++) {
        let num = Math.floor(Math.random() * arrRandom.length);
        str += arrRandom[num];
    }
    return str;
}
// $.ajax({
//     type: 'GET',
//     url: "https://vip.123pan.cn/1812611997/Be_as_gentle_as_ever/music.json",
//     success: function (res, heads, code) {
//         // console.log(JSON.parse(res));
//         console.log(res);
//     },
//     error: function () {
//         layer.msg(`<a href='JavaScript:;'style='color:#ff5722;'>${username}</a> 链接好像失效了`, {
//             icon: 5
//         });
//     }
// })
$("#music").on("click", ".music-menu .music-image .music-begin", function () {
    let src = $(this).attr("data-music");
    let music = document.getElementById("aud");
    music.src = src;
    let username = $(this).parent().parent().children(".music-info").children(".music-username").text();
    $("#music .music-menu .music-image i").removeClass("music-stop");
    $("#music .music-menu .music-image i").addClass("music-begin");
    $(this).removeClass("music-begin");
    $(this).addClass("music-stop");
    let str = random();
    $("#jindutiao1").attr("lay-filter", str);
    let req = new XMLHttpRequest();
    req.open("get", src, true);
    req.responseType = "blob"; // 加载二进制数据
    req.send();
    req.addEventListener("progress", function (oEvent) {
        if (oEvent.lengthComputable) {
            var percentComplete = oEvent.loaded / oEvent.total * 100;
            let element = layui.element;
            let util = layui.util;
            element.progress(str, parseInt(percentComplete) + '%');
        }
    });
    $.ajax({
        type: 'GET',
        url: src,
        success: function (res, heads, code) {
            if (code.status == 200) {
                document.title = musicTitle + " - " + username;
            }
        },
        error: function () {
            layer.msg(`<a href='JavaScript:;'style='color:#ff5722;'>${username}</a> 链接好像失效了`, {
                icon: 5
            });
        }
    })
    music.play();
})
$("#music").on("click", ".music-menu .music-image .music-stop", function () {
    let src = $(this).attr("data-music");
    let music = document.getElementById("aud");
    music.src = src;
    music.pause();
    $("#music .music-menu .music-image i").removeClass("music-stop");
    $("#music .music-menu .music-image i").addClass("music-begin");
    $(this).removeClass("music-stop");
    $(this).addClass("music-begin");
    document.title = musicTitle;
})
$("#music").on("click", ".music-menu .music-info .music-download", function () {
    let src = $(this).attr("data-music");
    let username = $(this).parent().children(".music-username").text();
    let str = random();
    $("#jindutiao1").attr("lay-filter", str);
    let req = new XMLHttpRequest();
    req.open("get", src, true);
    req.responseType = "blob"; // 加载二进制数据
    req.send();
    req.addEventListener("progress", function (oEvent) {
        if (oEvent.lengthComputable) {
            var percentComplete = oEvent.loaded / oEvent.total * 100;
            element.progress(str, parseInt(percentComplete) + '%');
        }
    });
    $.ajax({
        type: 'GET',
        url: src,
        success: function (res, heads, code) {
            if (code.status == 200) {
                let aler = layer.alert(`您确定要下载 <span class="down link-danger" lay-on="down">${username}</span> 吗?`, {
                    title: '下载',
                    closeBtn: 0,
                    anim: 5,
                    shadeClose: true,
                    skin: 'layui-layer-win10', // 2.8+
                    shade: 0.2,
                    // content: '123456',
                    btn: ['确定', '关闭'], //按钮
                    btn1: function () {
                        layer.close(aler);
                        window.open(src);
                    },
                    btn2: function () {
                        layer.close(aler);
                    }
                })
            }
        },
        error: function () {
            layer.msg(`<a href='JavaScript:;'style='color:#ff5722;'>${username}</a> 链接好像失效了`, {
                icon: 5
            });
        }
    })
})
setInterval(function () {
    let baifenbi = $("#jindutiao1 div").attr("lay-percent");
    if (baifenbi == "0%" || baifenbi == "100%") {
        $("#jindutiao1").fadeOut();
    } else {
        $("#jindutiao1").show();
    }
    var audio = document.getElementById("aud");
    // audio.duration; //获取总播放时间
    // audio.currentTime; //获取播放进度
    // console.log(audio.currentTime); //控制台显示
    // element.progress('jindutiao', parseInt(audio.duration) + '%');
    // console.log(audio.duration + '%');
}, 125);
//随机文案
$.ajax({
    url: 'https://v1.hitokoto.cn/?c=a&c=c', //一言开发者中心
    type: 'post',
    dataType: 'json',
    async: false,
    data: {
        format: 'json',
    },
    beforeSend: function () {
        //请求中执行的代码
    },
    complete: function () {
        //请求完成执行的代码
    },
    error: function () {
        //请求成功失败执行的代码
    },
    success: function (res) {
        // 状态码 200 表示请求成功
        // console.log(res);
        let hitokoto = res.hitokoto == null ? "" : `${res.hitokoto}`;
        let from = res.from == null ? "" : ` 出自：${res.from}`;
        let from_who = res.from_who == null ? "" : ` - ${res.from_who}`;
        $("#wenan").html(`<a href='JavaScript:;' class='ramp'>${hitokoto}${from}${from_who}</a>`);
    }
})
//设置
var form = layui.form;
window.localStorage.setItem("设置", JSON.stringify({
    "文案": "true",
}));
var 设置 = JSON.parse(window.localStorage.getItem("设置"));

function wenanDefault() {
    // layer.msg(设置.文案);
    if (设置.文案 == "true") {
        wenanShow();
    } else {
        wenanHide();
    }
}
wenanDefault();

function wenanHide() {
    $("#wenan").hide();
}

function wenanShow() {
    $("#wenan").show();
}
form.on('checkbox(文案)', function (data) {
    var elem = data.elem; // 获得 checkbox 原始 DOM 对象
    if (elem.checked) {
        wenanShow();
    } else {
        wenanHide();
    }
    // layer.msg('checked 状态: ' + elem.checked);
});
//圣诞树
$("#圣诞树").click(function () {
    layer.open({
        type: 1, // page 层类型
        area: ['700px', '500px'],
        title: '圣诞树',
        scrollbar: true,
        shade: 0.6, // 遮罩透明度
        shadeClose: true, // 点击遮罩区域，关闭弹层
        maxmin: true, // 允许全屏最小化
        anim: 0, // 0-6 的动画形式，-1 不开启
        content: `
            <video src='https://vip.123pan.cn/1812611997/%E8%A7%86%E9%A2%91/%E5%9C%A3%E8%AF%9E%E6%A0%91%E8%A7%86%E9%A2%91.mp4' autoplay="autoplay" loop="loop" style='width:100%;height:100%;'></video>`,
    });
})